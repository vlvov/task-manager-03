# TASK-MANAGER

## DEVELOPER

**NAME**: Vladimir Lvov

**E-MAIL**: vlvov@t1-consilting.com

## SOFTWARE

**JAVA**: JDK 1.8

**OS**: Windows 10 64bit

## HARDWARE

**CPU**: i5

**RAM**: 8Gb

**HDD**: 1000Gb

## APPLICATION RUN

```bash
java -jar ./task-manager.jar
```
